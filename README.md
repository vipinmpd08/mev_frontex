# MevFrontex

## Assignment
First fork this repository into your personal account and commit all the changes to your repository.

We would like you to implement this template.

  * Check template.png in the root directory.
  * HTML file is in `lib/mev_frontex_web/templates/page/index.html.eex` (check Elixir EEX for more information)
  * This template should run directly on [`localhost:4000`](http://localhost:4000)
  * Main Javascript file is in `assets/js/app.js`
  * The data in page should be dummy.
  * Feel free to use any JS framework.
  * Remember to use Webpack to bundle and NPM. webpack config file is in `assets/webpack.config.js`
  * Don't focus much on CSS.

The Features that we would like see.

  * Simple search
  * Data sorting

## How to start the server

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
